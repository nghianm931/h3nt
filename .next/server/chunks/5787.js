"use strict";
exports.id = 5787;
exports.ids = [5787];
exports.modules = {

/***/ 7549:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ navbarScroll)
/* harmony export */ });
function navbarScroll(navbar, isTransparent, isFaqPage) {
    if (!navbar) return;
    if (window.pageYOffset > 300) {
        navbar.classList.add("nav-scroll");
        if (isTransparent) navbar.classList.remove("bg-transparent");
    } else {
        navbar.classList.remove("nav-scroll");
        if (isTransparent) navbar.classList.add("bg-transparent");
    }
    window.addEventListener("scroll", ()=>{
        if (window.pageYOffset > 300) {
            navbar.classList.add("nav-scroll");
            if (isTransparent) navbar.classList.remove("bg-transparent");
        } else {
            navbar.classList.remove("nav-scroll");
            if (isTransparent) navbar.classList.add("bg-transparent");
        }
    });
}


/***/ }),

/***/ 1551:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ components_PreLoader)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
;// CONCATENATED MODULE: ./src/common/loadingPace.js
const loadingPace = ()=>{
    let preloader = document.querySelector("#preloader");
    if (!preloader) return;
    if (document.body.classList.contains("pace-done")) preloader.classList.add("isdone");
    document.addEventListener("load", ()=>preloader.classList.add("isdone"));
    if (typeof Pace === "undefined") return;
    Pace.on("start", ()=>preloader.classList.remove("isdone"));
    Pace.on("done", ()=>preloader.classList.add("isdone"));
};
/* harmony default export */ const common_loadingPace = (loadingPace);

;// CONCATENATED MODULE: ./src/components/PreLoader/index.jsx



const PreLoader = ()=>{
    (0,external_react_.useEffect)(()=>{
        setTimeout(()=>common_loadingPace(), 0);
    }, []);
    return /*#__PURE__*/ jsx_runtime_.jsx("div", {
        id: "preloader"
    });
};
/* harmony default export */ const components_PreLoader = (PreLoader);


/***/ }),

/***/ 8256:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ components_ScrollToTop)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
;// CONCATENATED MODULE: ./src/common/scrollToTop.js
function scrollToTop() {
    const toTop = document.querySelector(".to_top");
    if (!toTop) return;
    window.addEventListener("scroll", function() {
        var bodyScroll = window.scrollY;
        if (bodyScroll > 700) {
            toTop.classList.add("show");
        } else {
            toTop.classList.remove("show");
        }
    });
    toTop.addEventListener("click", function(event) {
        event.preventDefault();
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    });
}

;// CONCATENATED MODULE: ./src/components/ScrollToTop/index.jsx



const ScrollToTop = ({ topText  })=>{
    (0,external_react_.useEffect)(()=>{
        scrollToTop();
    }, []);
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
        href: "#",
        className: `to_top ${topText ? "" : "bg-gray rounded-circle icon-40 d-inline-flex align-items-center justify-content-center"}`,
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("i", {
                className: `bi bi-chevron-up ${topText ? "" : "fs-6 text-dark"}`
            }),
            topText && /*#__PURE__*/ jsx_runtime_.jsx("small", {
                children: "Top"
            })
        ]
    });
};
/* harmony default export */ const components_ScrollToTop = (ScrollToTop);


/***/ })

};
;